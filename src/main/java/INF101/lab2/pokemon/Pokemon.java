package INF101.lab2.pokemon;

import java.lang.Math;
import java.lang.annotation.Target;
import java.util.Random;
import java.util.Set;

import javax.lang.model.element.Name;

public class Pokemon implements IPokemon {
        String name;
        int healthPoints;
        int maxHealthPoints;
        int strength;
        Random random;
    
    public Pokemon(String name){
        this.name=name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        return healthPoints > 0;
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        
        target.damage(damageInflicted);
        
        if (target.isAlive()){
            System.out.println(name + " attacks " + target.getName() + ".");
        }
        else{
            System.out.println(target.getName() + " is defeated by " + name + ".");
        }
    }

    @Override
    public void damage(int damageTaken) {
        healthPoints = healthPoints-damageTaken;
        if (healthPoints < 0){
            healthPoints = 0;
        }
        if (damageTaken < 0){//om damage er lavere enn null 
            damageTaken = 0;//skal damage bli null
        }
        if(healthPoints > maxHealthPoints){
            healthPoints = maxHealthPoints;
        }

        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints);
    }
    @Override
    public String toString() {
        return name + " HP: " + "(" + healthPoints + "/" + maxHealthPoints +") STR: "+ strength;
    }

   
}
