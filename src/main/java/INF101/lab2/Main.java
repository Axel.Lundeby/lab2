package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated
        pokemon1 = new Pokemon("Jacob");
        pokemon2 = new Pokemon("Theodor");
        
        System.out.println(pokemon1);
        System.out.println(pokemon2 + "\n");

        while(pokemon1.isAlive() && pokemon2.isAlive()){
            pokemon1.attack(pokemon2);//pokemon1 en angriper pokemon 2
            pokemon2.attack(pokemon1);//pokemon2 en angriper pokemon 2
        }
    }
}
